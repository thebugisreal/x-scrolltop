﻿## Cài đặt

Yêu cầu: **jQuery**.

Clone về và nhúng hai file sau vào website của bạn.
```markdown
<link href="x-scrolltop.min.css" rel="stylesheet" />
```

```markdown
<script src="x-scrolltop.min.js"></script>
```

## Sử dụng

1. Tạo một thẻ với ID là `x-scrollTop`

```markdown
<a href="#" id="x-scrollTop"></a>
```
2. Chèn nội dung tùy ý vào bên trong thẻ này.

```markdown
<a href="#" id="x-scrollTop">↑</a>
```
3. Sử dụng (hoặc không) các attribute để tùy biến theo ý muốn.

| Attribute     | Mặc định  |  Nội dung                                |
| ------------- |:---------:| ---------------------------------------- |
| x-offset      | 500				| vị trí hiển thị button tính từ đầu trang |
| x-duration    | 500				| thời gian scroll tới đầu trang           |
| x-fade        | 500			  | thời gian fadeIn/fadeOut |

#### Ví dụ:

```markdown
<a href="#" id="x-scrollTop" x-offset="1000" x-duration="600" x-fade="400">↑</a>
```