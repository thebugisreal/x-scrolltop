﻿(function () {
	const obj = $('#x-scrollTop'),
	      offset = parseInt(obj.attr('x-offset')) || 500,
	      duration = parseInt(obj.attr('x-duration')) || 500,
				fade = parseInt(obj.attr('x-fade')) || 500;

	$(window).scroll(function () {
		if ($(this).scrollTop() > offset) {
			obj.fadeIn(fade);
		} else {
			obj.fadeOut(fade);
		}
	});

	obj.click(function (event) {
		event.preventDefault();
		$('html, body').animate({ scrollTop: 0 }, duration);
		return false;
	})
})();